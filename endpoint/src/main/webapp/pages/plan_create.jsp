<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Spring Security</title>
    <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/jumbotron-narrow.css" />" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="page-header">
        <h1>Create new plan</h1>
    </div>
    <form:form method="POST" action="resultPlan">
        <table>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="name">name</form:label></td>
                <td><form:input id="name" class="form-control" path="name"/></td>
            </tr>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="details">details</form:label></td>
                <td><form:input id="details" class="form-control" path="details"/></td>
            </tr>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="maxSeats">max Seats</form:label></td>
                <td><form:input id="maxseats" class="form-control" path="maxSeats"/></td>
            </tr>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="minSeats">min Seats</form:label></td>
                <td><form:input id="maxseats" class="form-control" path="minSeats"/></td>
            </tr>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="feePerSeat">fee Per Seat</form:label></td>
                <td><form:input id="feeperseat" class="form-control" path="feePerSeat"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Add new Plan</button>
                </td>
            </tr>
        </table>
    </form:form>
    <p>${message}</p>
</div>
</body>
</html>