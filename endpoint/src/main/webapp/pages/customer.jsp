<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Spring Security</title>

    <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">

    <link href="<c:url value="/pages/css/jumbotron-narrow.css" />" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <h2>Customer page</h2>
    <p>Here you cam see all customer information, plans and subscriptions</p>
    <ul class="nav nav-pills">
        <li class="active"><a data-toggle="pill" href="#customerinfo">Information</a></li>
        <li><a data-toggle="pill" href="#customerPlan">Plans</a></li>
        <li><a data-toggle="pill" href="#subscriptions">Subscriptions</a></li>
    </ul>
    <div class="tab-content">
        <div id="customerinfo" class="tab-pane fade in active">
            <h3>Information</h3>
            <p>Login: ${login}</p>
        </div>
        <div id="customerPlan" class="tab-pane fade">
            <h3>Plans</h3>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>name</th>
                    <th>details</th>
                    <th>maxSeats</th>
                    <th>minSeats</th>
                    <th>feePerSeat</th>
                    <th>buyPlan</th>
                </tr>
                </thead>

                <c:forEach var="plan" items="${plans}">
                    <tr>
                        <td>${plan.name}</td>
                        <td>${plan.details}</td>
                        <td>${plan.maxSeats}</td>
                        <td>${plan.minSeats}</td>
                        <td>${plan.feePerSeat}</td>
                        <td><a class="btn btn-primary btn-sm" href="<c:url value="/customer/buyPlan?id=${plan.id}" />"
                               role="button">buy plan</a></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div id="subscriptions" class="tab-pane fade">
            <h3>Subscriptions</h3>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>plan id</th>
                    <th>customer id</th>
                    <th>used seats</th>
                    <th>unassign</th>
                </tr>
                </thead>

                <c:forEach var="subscription" items="${subscriptions}">
                    <tr>
                        <td>${subscription.planId}</td>
                        <td>${subscription.customerId}</td>
                        <td>${subscription.usedSeats}</td>
                        <td><a class="btn btn-primary btn-sm"
                               href="<c:url value="/customer/unassign?id=${subscription.id}" />"
                               role="button">unassign</a></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
</body>
</html>
