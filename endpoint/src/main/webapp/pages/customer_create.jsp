<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Spring Security</title>
    <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/jumbotron-narrow.css" />" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="page-header">
        <h1>Create new customer</h1>
    </div>
    <form:form method="POST" action="resultCustomer">
        <table>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="firstName">firstName</form:label></td>
                <td><form:input id="firstname" class="form-control" path="firstName"/></td>
            </tr>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="lastName">lastName</form:label></td>
                <td><form:input id="lastname" class="form-control" path="lastName"/></td>
            </tr>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="login">login</form:label></td>
                <td><form:input id="login" class="form-control" path="login"/></td>
            </tr>
            <tr>
                <td><form:label class="col-lg-2 control-label" path="pass">pass</form:label></td>
                <td><form:input id="pass" class="form-control" path="pass"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <button id="create" class="btn btn-lg btn-primary btn-block" type="submit">Add new Customer</button>
                </td>
            </tr>
        </table>
    </form:form>
    <p>${message}</p>
</div>
</body>
</html>