<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Spring Security</title>

    <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">

    <link href="<c:url value="/pages/css/jumbotron-narrow.css" />" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">
    <h2>Admin page</h2>
    <p>Here you cam see admin information: customer list, admin list</p>
    <ul class="nav nav-pills">
        <li class="active"><a data-toggle="pill" href="#customerList">Customer list</a></li>
        <li><a data-toggle="pill" href="#AdminList">Admin List</a></li>
        <li><a data-toggle="pill" href="#PlanList">Plan list</a> </li>
    </ul>
    <div class="tab-content">
        <div id="customerList" class="tab-pane fade in active">
            <h3>Customer list</h3>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>FirstName</th>
                    <th>LastName</th>
                    <th>Login</th>
                    <th>Password</th>
                    <th>Money</th>
                </tr>
                </thead>

                <c:forEach var="customer" items="${users}">
                    <tr>
                        <td>${customer.id}</td>
                        <td>${customer.firstName}</td>
                        <td>${customer.lastName}</td>
                        <td>${customer.login}</td>
                        <td>${customer.pass}</td>
                        <td>${customer.money}</td>
                    </tr>
                </c:forEach>
            </table>

            <p><a id="createcustomer" class="btn btn-lg btn-primary" href="<c:url value="/admin/addCustomer" />" role="button">Create new
                customer</a></p>

        </div>
        <div id="AdminList" class="tab-pane fade">
            <h3>AdminList</h3>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>FirstName</th>
                    <th>LastName</th>
                    <th>Login</th>
                    <th>Password</th>
                </tr>
                </thead>

                <c:forEach var="admin" items="${admins}">
                    <tr>
                        <td>${admin.id}</td>
                        <td>${admin.firstName}</td>
                        <td>${admin.lastName}</td>
                        <td>${admin.login}</td>
                        <td>${admin.pass}</td>
                    </tr>
                </c:forEach>
            </table>
            <p><a id="createadmin" class="btn btn-lg btn-primary" href="<c:url value="/admin/addAdmin" />" role="button">Create new
                admin</a></p>
        </div>
        <div id="PlanList" class="tab-pane fade">
            <h3>Plans</h3>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>name</th>
                    <th>details</th>
                    <th>maxSeats</th>
                    <th>minSeats</th>
                    <th>feePerSeat</th>
                </tr>
                </thead>

                <c:forEach var="plan" items="${plans}">
                    <tr>
                        <td>${plan.name}</td>
                        <td>${plan.details}</td>
                        <td>${plan.maxSeats}</td>
                        <td>${plan.minSeats}</td>
                        <td>${plan.feePerSeat}</td>
                    </tr>
                </c:forEach>
            </table>
            <p><a id="createplan" class="btn btn-lg btn-primary" href="<c:url value="/admin/addPlan" />" role="button">Add plan</a></p>
        </div>
    </div>
</div>
</body>
</html>
