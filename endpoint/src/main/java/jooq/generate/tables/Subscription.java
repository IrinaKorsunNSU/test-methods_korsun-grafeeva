/**
 * This class is generated by jOOQ
 */
package jooq.generate.tables;

/**
 * This class is generated by jOOQ.
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.3.2" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Subscription extends org.jooq.impl.TableImpl<jooq.generate.tables.records.SubscriptionRecord> {

	private static final long serialVersionUID = 1513302174;

	/**
	 * The singleton instance of <code>IRINALABS.SUBSCRIPTION</code>
	 */
	public static final jooq.generate.tables.Subscription SUBSCRIPTION = new jooq.generate.tables.Subscription();

	/**
	 * The class holding records for this type
	 */
	@Override
	public java.lang.Class<jooq.generate.tables.records.SubscriptionRecord> getRecordType() {
		return jooq.generate.tables.records.SubscriptionRecord.class;
	}

	/**
	 * The column <code>IRINALABS.SUBSCRIPTION.ID</code>.
	 */
	public final org.jooq.TableField<jooq.generate.tables.records.SubscriptionRecord, java.lang.Object> ID = createField("ID", org.jooq.impl.SQLDataType.OTHER, this, "");

	/**
	 * The column <code>IRINALABS.SUBSCRIPTION.PLAN_ID</code>.
	 */
	public final org.jooq.TableField<jooq.generate.tables.records.SubscriptionRecord, java.lang.Object> PLAN_ID = createField("PLAN_ID", org.jooq.impl.SQLDataType.OTHER, this, "");

	/**
	 * The column <code>IRINALABS.SUBSCRIPTION.CUSTOMER_ID</code>.
	 */
	public final org.jooq.TableField<jooq.generate.tables.records.SubscriptionRecord, java.lang.Object> CUSTOMER_ID = createField("CUSTOMER_ID", org.jooq.impl.SQLDataType.OTHER, this, "");

	/**
	 * The column <code>IRINALABS.SUBSCRIPTION.USED_SEATS</code>.
	 */
	public final org.jooq.TableField<jooq.generate.tables.records.SubscriptionRecord, java.lang.Object> USED_SEATS = createField("USED_SEATS", org.jooq.impl.SQLDataType.OTHER, this, "");

	/**
	 * Create a <code>IRINALABS.SUBSCRIPTION</code> table reference
	 */
	public Subscription() {
		this("SUBSCRIPTION", null);
	}

	/**
	 * Create an aliased <code>IRINALABS.SUBSCRIPTION</code> table reference
	 */
	public Subscription(java.lang.String alias) {
		this(alias, jooq.generate.tables.Subscription.SUBSCRIPTION);
	}

	private Subscription(java.lang.String alias, org.jooq.Table<jooq.generate.tables.records.SubscriptionRecord> aliased) {
		this(alias, aliased, null);
	}

	private Subscription(java.lang.String alias, org.jooq.Table<jooq.generate.tables.records.SubscriptionRecord> aliased, org.jooq.Field<?>[] parameters) {
		super(alias, jooq.generate.Irinalabs.IRINALABS, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.UniqueKey<jooq.generate.tables.records.SubscriptionRecord> getPrimaryKey() {
		return jooq.generate.Keys.PK_SUBSCRIPTION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.util.List<org.jooq.UniqueKey<jooq.generate.tables.records.SubscriptionRecord>> getKeys() {
		return java.util.Arrays.<org.jooq.UniqueKey<jooq.generate.tables.records.SubscriptionRecord>>asList(jooq.generate.Keys.PK_SUBSCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public jooq.generate.tables.Subscription as(java.lang.String alias) {
		return new jooq.generate.tables.Subscription(alias, this);
	}

	/**
	 * Rename this table
	 */
	public jooq.generate.tables.Subscription rename(java.lang.String name) {
		return new jooq.generate.tables.Subscription(name, null);
	}
}
