/**
 * This class is generated by jOOQ
 */
package jooq.generate.tables;

/**
 * This class is generated by jOOQ.
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.3.2" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserDb extends org.jooq.impl.TableImpl<jooq.generate.tables.records.UserDbRecord> {

	private static final long serialVersionUID = -820150413;

	/**
	 * The singleton instance of <code>IRINALABS.USER_DB</code>
	 */
	public static final jooq.generate.tables.UserDb USER_DB = new jooq.generate.tables.UserDb();

	/**
	 * The class holding records for this type
	 */
	@Override
	public java.lang.Class<jooq.generate.tables.records.UserDbRecord> getRecordType() {
		return jooq.generate.tables.records.UserDbRecord.class;
	}

	/**
	 * The column <code>IRINALABS.USER_DB.ID</code>.
	 */
	public final org.jooq.TableField<jooq.generate.tables.records.UserDbRecord, java.lang.Object> ID = createField("ID", org.jooq.impl.SQLDataType.OTHER, this, "");

	/**
	 * The column <code>IRINALABS.USER_DB.LOGIN</code>.
	 */
	public final org.jooq.TableField<jooq.generate.tables.records.UserDbRecord, java.lang.Object> LOGIN = createField("LOGIN", org.jooq.impl.SQLDataType.OTHER, this, "");

	/**
	 * The column <code>IRINALABS.USER_DB.USER_ROLE</code>.
	 */
	public final org.jooq.TableField<jooq.generate.tables.records.UserDbRecord, java.lang.Object> USER_ROLE = createField("USER_ROLE", org.jooq.impl.SQLDataType.OTHER, this, "");

	/**
	 * Create a <code>IRINALABS.USER_DB</code> table reference
	 */
	public UserDb() {
		this("USER_DB", null);
	}

	/**
	 * Create an aliased <code>IRINALABS.USER_DB</code> table reference
	 */
	public UserDb(java.lang.String alias) {
		this(alias, jooq.generate.tables.UserDb.USER_DB);
	}

	private UserDb(java.lang.String alias, org.jooq.Table<jooq.generate.tables.records.UserDbRecord> aliased) {
		this(alias, aliased, null);
	}

	private UserDb(java.lang.String alias, org.jooq.Table<jooq.generate.tables.records.UserDbRecord> aliased, org.jooq.Field<?>[] parameters) {
		super(alias, jooq.generate.Irinalabs.IRINALABS, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public jooq.generate.tables.UserDb as(java.lang.String alias) {
		return new jooq.generate.tables.UserDb(alias, this);
	}

	/**
	 * Rename this table
	 */
	public jooq.generate.tables.UserDb rename(java.lang.String name) {
		return new jooq.generate.tables.UserDb(name, null);
	}
}
