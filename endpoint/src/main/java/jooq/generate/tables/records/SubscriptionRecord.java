/**
 * This class is generated by jOOQ
 */
package jooq.generate.tables.records;

/**
 * This class is generated by jOOQ.
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.3.2" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SubscriptionRecord extends org.jooq.impl.UpdatableRecordImpl<jooq.generate.tables.records.SubscriptionRecord> implements org.jooq.Record4<java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object>, jooq.generate.tables.interfaces.ISubscription {

	private static final long serialVersionUID = 803045756;

	/**
	 * Setter for <code>IRINALABS.SUBSCRIPTION.ID</code>.
	 */
	@Override
	public void setId(java.lang.Object value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>IRINALABS.SUBSCRIPTION.ID</code>.
	 */
	@Override
	public java.lang.Object getId() {
		return (java.lang.Object) getValue(0);
	}

	/**
	 * Setter for <code>IRINALABS.SUBSCRIPTION.PLAN_ID</code>.
	 */
	@Override
	public void setPlanId(java.lang.Object value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>IRINALABS.SUBSCRIPTION.PLAN_ID</code>.
	 */
	@Override
	public java.lang.Object getPlanId() {
		return (java.lang.Object) getValue(1);
	}

	/**
	 * Setter for <code>IRINALABS.SUBSCRIPTION.CUSTOMER_ID</code>.
	 */
	@Override
	public void setCustomerId(java.lang.Object value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>IRINALABS.SUBSCRIPTION.CUSTOMER_ID</code>.
	 */
	@Override
	public java.lang.Object getCustomerId() {
		return (java.lang.Object) getValue(2);
	}

	/**
	 * Setter for <code>IRINALABS.SUBSCRIPTION.USED_SEATS</code>.
	 */
	@Override
	public void setUsedSeats(java.lang.Object value) {
		setValue(3, value);
	}

	/**
	 * Getter for <code>IRINALABS.SUBSCRIPTION.USED_SEATS</code>.
	 */
	@Override
	public java.lang.Object getUsedSeats() {
		return (java.lang.Object) getValue(3);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Record1<java.lang.Object> key() {
		return (org.jooq.Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record4 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Row4<java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object> fieldsRow() {
		return (org.jooq.Row4) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Row4<java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object> valuesRow() {
		return (org.jooq.Row4) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Object> field1() {
		return jooq.generate.tables.Subscription.SUBSCRIPTION.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Object> field2() {
		return jooq.generate.tables.Subscription.SUBSCRIPTION.PLAN_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Object> field3() {
		return jooq.generate.tables.Subscription.SUBSCRIPTION.CUSTOMER_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Object> field4() {
		return jooq.generate.tables.Subscription.SUBSCRIPTION.USED_SEATS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Object value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Object value2() {
		return getPlanId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Object value3() {
		return getCustomerId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Object value4() {
		return getUsedSeats();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SubscriptionRecord value1(java.lang.Object value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SubscriptionRecord value2(java.lang.Object value) {
		setPlanId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SubscriptionRecord value3(java.lang.Object value) {
		setCustomerId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SubscriptionRecord value4(java.lang.Object value) {
		setUsedSeats(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SubscriptionRecord values(java.lang.Object value1, java.lang.Object value2, java.lang.Object value3, java.lang.Object value4) {
		return this;
	}

	// -------------------------------------------------------------------------
	// FROM and INTO
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void from(jooq.generate.tables.interfaces.ISubscription from) {
		setId(from.getId());
		setPlanId(from.getPlanId());
		setCustomerId(from.getCustomerId());
		setUsedSeats(from.getUsedSeats());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends jooq.generate.tables.interfaces.ISubscription> E into(E into) {
		into.from(this);
		return into;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached SubscriptionRecord
	 */
	public SubscriptionRecord() {
		super(jooq.generate.tables.Subscription.SUBSCRIPTION);
	}

	/**
	 * Create a detached, initialised SubscriptionRecord
	 */
	public SubscriptionRecord(java.lang.Object id, java.lang.Object planId, java.lang.Object customerId, java.lang.Object usedSeats) {
		super(jooq.generate.tables.Subscription.SUBSCRIPTION);

		setValue(0, id);
		setValue(1, planId);
		setValue(2, customerId);
		setValue(3, usedSeats);
	}
}
