package ru.nsu.fit.endpoint.webserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.nsu.fit.endpoint.appserver.service.AdminService;
import ru.nsu.fit.endpoint.appserver.service.PlanService;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Admin;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Customer;
import ru.nsu.fit.endpoint.appserver.service.CustomerService;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.QuantityExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Plan;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    CustomerService customerService;

    @Autowired
    AdminService adminService;

    @Autowired
    PlanService planService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.GET)
    public String showAllUsers(Model model) {
        model.addAttribute("users", customerService.findAll());
        model.addAttribute("admins", adminService.findAll());
        model.addAttribute("plans", planService.getAllPlans());
        return "admin";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/addCustomer", method = RequestMethod.GET)
    public ModelAndView addUser() throws PatternMatchExeption, QuantityExeption, SecurityPolicyExeption {
        return new ModelAndView("customer_create", "command", new Customer());
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/addAdmin", method = RequestMethod.GET)
    public ModelAndView addAdmin() throws PatternMatchExeption, QuantityExeption, SecurityPolicyExeption {
        return new ModelAndView("admin_create", "command", new Admin());
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/addPlan", method = RequestMethod.GET)
    public ModelAndView addPlan() throws PatternMatchExeption, QuantityExeption, SecurityPolicyExeption {
        return new ModelAndView("plan_create", "command", new Plan());
    }

    @RequestMapping(value = "/resultCustomer", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addToBase(@ModelAttribute("secure") Customer customer, Model model) {
        try {
            Customer new_customer = new Customer(customer.getFirstName(),
                    customer.getLastName(),
                    customer.getLogin(),
                    customer.getPass(), 0);
            customerService.insert(
                    new_customer.getFirstName(),
                    new_customer.getLastName(),
                    new_customer.getLogin(),
                    new_customer.getPass(), 0);
        } catch (PatternMatchExeption patternMatchExeption) {
            model.addAttribute("message", patternMatchExeption.getMessage());
            return new ModelAndView("customer_create", "command", new Customer());
        } catch (QuantityExeption quantityExeption) {
            model.addAttribute("message", quantityExeption.getMessage());
            return new ModelAndView("customer_create", "command", new Customer());
        } catch (SecurityPolicyExeption securityPolicyExeption) {
            model.addAttribute("message", securityPolicyExeption.getMessage());
            return new ModelAndView("customer_create", "command", new Customer());
        }
        return new ModelAndView("redirect:/admin");
    }

    @RequestMapping(value = "/resultAdmin", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addToBase(@ModelAttribute("secure") Admin admin, Model model) {
        try {
            Admin new_admin = new Admin(admin.getFirstName(),
                    admin.getLastName(),
                    admin.getLogin(),
                    admin.getPass());
            adminService.insert(
                    new_admin.getFirstName(),
                    new_admin.getLastName(),
                    new_admin.getLogin(),
                    new_admin.getPass());
        } catch (PatternMatchExeption patternMatchExeption) {
            model.addAttribute("message", patternMatchExeption.getMessage());
            return new ModelAndView("admin_create", "command", new Customer());
        } catch (SecurityPolicyExeption securityPolicyExeption) {
            model.addAttribute("message", securityPolicyExeption.getMessage());
            return new ModelAndView("admin_create", "command", new Customer());
        }
        return new ModelAndView("redirect:/admin");
    }

    @RequestMapping(value = "/resultPlan", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addToBase(@ModelAttribute("secure") Plan plan) {
        planService.insert(plan);
        return new ModelAndView("redirect:/admin");
    }
}
