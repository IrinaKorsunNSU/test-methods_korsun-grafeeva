package ru.nsu.fit.endpoint.appserver.service.database.exeptions;

/**
 * Created by Irina Korsun on 13.10.2016.
 */
public class SecurityPolicyExeption extends Exception {
    public SecurityPolicyExeption(String s) {
        super(s);
    }
}
