package ru.nsu.fit.endpoint.appserver.service;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static jooq.generate.Tables.ADMIN_DB;
import static jooq.generate.Tables.CUSTOMER;
import static jooq.generate.Tables.USER_DB;

@Service
@Transactional("transactionManager")
public class AdminService {

    @Autowired
    private DSLContext dsl;

    @Autowired
    private ShaPasswordEncoder encoder;

    @Secured("ROLE_ADMIN")
    public List<Record> findAll() {
        return dsl.select().from(ADMIN_DB).fetch();
    }

    @Secured("ROLE_ADMIN")
    public void insert(String firstName, String lastName, String login, String pass) {
        dsl.insertInto(ADMIN_DB, ADMIN_DB.FIRST_NAME, ADMIN_DB.LAST_NAME, ADMIN_DB.LOGIN, ADMIN_DB.PASS)
                .values(firstName, lastName, login, encoder.encodePassword(pass, null))
                .execute();
        dsl.insertInto(USER_DB, USER_DB.ID, USER_DB.LOGIN, USER_DB.USER_ROLE)
                .values(dsl.select(ADMIN_DB.ID)
                        .from(ADMIN_DB)
                        .where(ADMIN_DB.LOGIN.eq(login))
                        .fetch(), login, "ADMIN");
    }
}
