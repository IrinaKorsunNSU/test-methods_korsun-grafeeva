package ru.nsu.fit.endpoint.webserver.controller;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.nsu.fit.endpoint.appserver.service.CustomerService;
import ru.nsu.fit.endpoint.appserver.service.PlanService;
import ru.nsu.fit.endpoint.appserver.service.SubscriptionService;

import static jooq.generate.tables.Subscription.SUBSCRIPTION;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;
    @Autowired
    PlanService planService;
    @Autowired
    SubscriptionService subscriptionService;
    @Autowired
    DSLContext dsl;

    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(method = RequestMethod.GET)
    public String showCustomerInfo(Model model) {
        Authentication credentials = customerService.findByLogin();
        model.addAttribute("login", credentials.getName());
        model.addAttribute("plans", planService.getAllPlans());
        model.addAttribute("subscriptions", subscriptionService.getAllCustomerSubscriptions());
        return "customer";
    }

    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/unassign", method = RequestMethod.GET)
    public ModelAndView unassign(@RequestParam(value = "id") String id) {
        subscriptionService.unassign(id);
        return new ModelAndView(new RedirectView("/customer#subscriptions", true));
    }

    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/buyPlan", method = RequestMethod.GET)
    public ModelAndView buyPlan(@RequestParam(value = "id") String id) {
        planService.enqueuedingPlan(id);
        return new ModelAndView(new RedirectView("/customer#plans", true));
    }
}
