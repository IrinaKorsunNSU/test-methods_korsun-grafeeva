package ru.nsu.fit.endpoint.appserver.service.entity.data;

import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.QuantityExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;
import ru.nsu.fit.endpoint.appserver.service.database.validation.CustomerValidator;
import ru.nsu.fit.endpoint.appserver.service.entity.enums.UserRoleEnum;

import java.util.UUID;

public class Customer extends CommonUser {

    private int money;

    private CustomerValidator validator;

    public Customer(String firstName, String lastName, String login, String pass, int money) throws PatternMatchExeption,
            QuantityExeption, SecurityPolicyExeption {
        super(firstName, lastName, login, pass);
        this.money = money;
        this.userRole = UserRoleEnum.CUSTOMER.name();
        validator = new CustomerValidator(firstName, lastName, login, pass, money);
        validator.validate();
    }

    public Customer() {
        super();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

    public int getMoney() {
        return money;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setValidator(CustomerValidator validator) {
        this.validator = validator;
    }
}
