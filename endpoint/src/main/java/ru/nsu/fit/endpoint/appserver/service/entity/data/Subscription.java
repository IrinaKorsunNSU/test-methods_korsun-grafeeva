package ru.nsu.fit.endpoint.appserver.service.entity.data;


import ru.nsu.fit.endpoint.appserver.service.database.validation.SubscriptionValidator;


public class Subscription {
    private Integer customerId;
    private Integer servicePlanId;
    private int usedSeats;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getServicePlanId() {
        return servicePlanId;
    }

    public void setServicePlanId(Integer servicePlanId) {
        this.servicePlanId = servicePlanId;
    }

    public int getUsedSeats() {
        return usedSeats;
    }

    public void setUsedSeats(int usedSeats) {
        this.usedSeats = usedSeats;
    }


    private SubscriptionValidator validator;

    public Subscription(Integer customerId, Integer servicePlanId, int usedSeats) {
        this.customerId = customerId;
        this.servicePlanId = servicePlanId;
        this.usedSeats = usedSeats;

        validator = new SubscriptionValidator(customerId, servicePlanId, usedSeats);
        validator.validate();
    }
}
