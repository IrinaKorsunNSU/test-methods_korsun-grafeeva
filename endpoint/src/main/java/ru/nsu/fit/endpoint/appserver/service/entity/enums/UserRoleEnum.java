package ru.nsu.fit.endpoint.appserver.service.entity.enums;

public enum UserRoleEnum {

    ADMIN,
    USER,
    ANONYMOUS,
    CUSTOMER;

    UserRoleEnum() {
    }

}