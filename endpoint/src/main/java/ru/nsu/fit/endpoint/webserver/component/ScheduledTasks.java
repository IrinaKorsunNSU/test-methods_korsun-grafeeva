package ru.nsu.fit.endpoint.webserver.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.nsu.fit.endpoint.appserver.service.PlanService;

@Component
public class ScheduledTasks {

    @Autowired
    PlanService planService;

    @Scheduled(fixedRate = 5000)
    public void processingPlans() {
        planService.processingPlan();
    }
}