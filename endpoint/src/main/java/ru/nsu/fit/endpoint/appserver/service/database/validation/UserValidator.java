package ru.nsu.fit.endpoint.appserver.service.database.validation;

import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator {
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,12})";
    private static final String NAME_PATTERN =
            "(([A-Z])([a-z]){2,12}$)";
    private static final String LOGIN_PATTERN =
            "^[a-z0-9](\\.?[a-z0-9_-]){0,}@[a-z0-9-]+\\.([a-z]{1,6}\\.)?[a-z]{2,6}$";

    private Pattern passwordPattern;
    private Pattern namePattern;
    private Pattern loginPattern;
    private Matcher matcher;

    public UserValidator(String firstName, String lastName, String login, String pass) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
        passwordPattern = Pattern.compile(PASSWORD_PATTERN);
        namePattern = Pattern.compile(NAME_PATTERN);
        loginPattern = Pattern.compile(LOGIN_PATTERN);
    }

    public void validate() throws PatternMatchExeption, SecurityPolicyExeption {
        passwordValidation();
        nameValiation();
        loginValidation();
    }

    private void nameValiation() throws PatternMatchExeption {
        if (!nameValidator(lastName)) {
            throw new PatternMatchExeption("lastName's length should be more or equal 2 symbols and less or equal 12 symbols, " +
                    "starts with Upper case and doesn't contain digests and whitespaces");
        }
        if (!nameValidator(firstName)) {
            throw new PatternMatchExeption("firstName's length should be more or equal 2 symbols and less or equal 12 symbols, " +
                    "starts with Upper case and doesn't contain digests and whitespaces");
        }
    }

    private void passwordValidation() throws PatternMatchExeption, SecurityPolicyExeption {
        if (!passwordValidator()) {
            throw new PatternMatchExeption("Password's length should be more or equal 6 symbols and less then 13," +
                    " should have upper case letter, digits and special symbols");
        }
        if (pass.toLowerCase().contains(login.toLowerCase())) {
            throw new SecurityPolicyExeption("Password contains login");
        }
        if (pass.toLowerCase().contains(firstName.toLowerCase())) {
            throw new SecurityPolicyExeption("Password contains firstName");
        }
        if (pass.toLowerCase().contains(lastName.toLowerCase())) {
            throw new SecurityPolicyExeption("Password contains lastName");
        }
    }

    private void loginValidation() throws PatternMatchExeption {
        if (!loginValidator()) {
            throw new PatternMatchExeption("Incorrect login");
        }
    }

    private boolean passwordValidator() {
        matcher = passwordPattern.matcher(pass);
        return matcher.matches();
    }

    private boolean nameValidator(String name) {
        matcher = namePattern.matcher(name);
        return matcher.matches();
    }

    public boolean loginValidator() {
        matcher = loginPattern.matcher(login);
        return matcher.matches();
    }

}
