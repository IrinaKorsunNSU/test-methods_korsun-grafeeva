package ru.nsu.fit.endpoint.appserver.service;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Customer;
import ru.nsu.fit.endpoint.appserver.service.entity.enums.UserRoleEnum;


import static jooq.generate.Tables.CUSTOMER;
import static jooq.generate.Tables.USER_DB;

@Service
@Transactional("transactionManager")
public class CustomerService {
    @Autowired
    private DSLContext dsl;

    @Autowired
    private ShaPasswordEncoder encoder;

    @Secured("ROLE_ADMIN")
    public Result<Record> findAll() {
        return dsl.select().from(CUSTOMER).fetch();
    }

    @Secured("ROLE_ADMIN")
    public void insert(String firstName, String lastName, String login, String pass, int money) {
        dsl.insertInto(CUSTOMER, CUSTOMER.FIRST_NAME, CUSTOMER.LAST_NAME, CUSTOMER.LOGIN, CUSTOMER.PASS, CUSTOMER.MONEY)
                .values(firstName, lastName, login, encoder.encodePassword(pass, null), money)
                .execute();
        dsl.insertInto(USER_DB, USER_DB.ID, USER_DB.LOGIN, USER_DB.USER_ROLE)
                .values(dsl.select(CUSTOMER.ID)
                        .from(CUSTOMER)
                        .where(CUSTOMER.LOGIN.eq(login))
                        .fetch(), login, UserRoleEnum.CUSTOMER.name());
    }

    @Secured("ROLE_CUSTOMER")
    public Authentication findByLogin() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Secured("ROLE_CUSTOMER")
    public Integer getId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return dsl.select(CUSTOMER.ID).from(CUSTOMER).where(CUSTOMER.LOGIN.eq(auth.getName())).execute();
    }
}
