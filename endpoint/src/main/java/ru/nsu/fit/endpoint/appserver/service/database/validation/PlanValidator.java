package ru.nsu.fit.endpoint.appserver.service.database.validation;

import org.apache.commons.lang.Validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlanValidator {
    private String name;
    private String details;
    private int maxSeats;
    private int minSeats;
    private int feePerUnit;


    private static final String NAME_PATTERN =
            "[A-Za-z0-9]{2,128}$";

    private Pattern namePattern;


    public PlanValidator(String name, String details, int maxSeats, int minSeats, int feePerUnit) {
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;

        namePattern = Pattern.compile(NAME_PATTERN);
    }

    public void validate() {
        Validate.isTrue(details.length() >= 1 && details.length() <= 1024, "details's length should be more or equal 1 symbols and less or equal 1024 symbols");
        Validate.isTrue(validateName(), "name's length should be more or equal 2 symbols and less or equal 128 symbols" + " and haven't contains special symbols");
        Validate.isTrue(maxSeats > 0 && maxSeats < 999999, "max seats should be more or equal 1 symbols and less or equal 999999");
        Validate.isTrue(minSeats > 0 && minSeats < 999999 && minSeats <= maxSeats, "min seats should be more or equal 1 symbols and " +
                "less or equal 999999 and min seats should be less or equal max seats");
        Validate.isTrue(feePerUnit >= 0 && feePerUnit <= 999999, "fee per unit should be more or equal 0 symbols and less or equal 999999");
    }

    private boolean validateName() {
        Matcher matcher = namePattern.matcher(name);
        return matcher.matches();
    }
}
