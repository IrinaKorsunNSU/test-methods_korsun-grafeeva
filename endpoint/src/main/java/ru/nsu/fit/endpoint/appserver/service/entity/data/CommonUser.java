package ru.nsu.fit.endpoint.appserver.service.entity.data;

public class CommonUser {
    public String firstName;
    public String lastName;
    public String login;
    public String pass;
    public String userRole;

    CommonUser(String firstName, String lastName, String login, String pass) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
    }
    CommonUser(){

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

}
