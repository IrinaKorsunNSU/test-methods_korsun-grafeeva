package ru.nsu.fit.endpoint.appserver.service;

import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;
import ru.nsu.fit.endpoint.appserver.service.entity.data.CommonUser;

public interface UserService {

    CommonUser getUser(String login) throws SecurityPolicyExeption, PatternMatchExeption;
}