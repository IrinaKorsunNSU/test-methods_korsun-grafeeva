package ru.nsu.fit.endpoint.appserver.service;

import jooq.generate.tables.pojos.PlansInPending;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Plan;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Subscription;

import java.math.BigDecimal;
import java.util.List;

import static jooq.generate.Tables.PLAN;
import static jooq.generate.Tables.PLANS_IN_PENDING;
import static jooq.generate.Tables.SUBSCRIPTION;

@Service
@Transactional("transactionManager")
public class PlanService {
    @Autowired
    private DSLContext dsl;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Secured("ROLE_CUSTOMER")
    public List<Record> getAllCustomerPlans() {
        return dsl.select()
                .from(PLAN)
                .where(PLAN.ID
                        .in(dsl.select(SUBSCRIPTION.PLAN_ID)
                                .from(SUBSCRIPTION)
                                .where(SUBSCRIPTION.CUSTOMER_ID
                                        .eq(customerService.getId()))))
                .fetch();
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    public List<Record> getAllPlans() {
        return dsl.select().from(PLAN).fetch();
    }

    @Secured("ROLE_ADMIN")
    public void insert(Plan plan) {
        dsl.insertInto(PLAN, PLAN.NAME, PLAN.DETAILS, PLAN.MAX_SEATS, PLAN.MIN_SEATS, PLAN.FEE_PER_SEAT)
                .values(plan.getName(), plan.getDetails(), plan.getMaxSeats(), plan.getMinSeats(), plan.getFeePerSeat())
                .execute();
    }

    @Rollback
    @Secured("ROLE_CUSTOMER")
    public void enqueuedingPlan(String plan_id) {
        dsl.insertInto(PLANS_IN_PENDING, PLANS_IN_PENDING.PLAN_ID, PLANS_IN_PENDING.CUSTOMER_ID)
                .values(plan_id, customerService.getId()).execute();
        dsl.deleteFrom(PLAN).where(PLAN.ID.eq(plan_id)).execute();
    }

    @Rollback
    public void processingPlan() {
        List<PlansInPending> list = dsl.selectFrom(PLANS_IN_PENDING).fetchInto(PlansInPending.class);
        for (PlansInPending plan : list) {
            Subscription subscription = new Subscription(((BigDecimal) plan.getCustomerId()).intValue(),
                    ((BigDecimal) plan.getPlanId()).intValue(),
                    1);
            subscriptionService.insert(subscription);
        }
        dsl.deleteFrom(PLANS_IN_PENDING).execute();
    }
}
