package ru.nsu.fit.endpoint.appserver.service.database.validation;


import org.apache.commons.lang.Validate;

import java.util.UUID;

public class SubscriptionValidator {
    private Integer customerId;
    private Integer servicePlanId;
    private int usedSeats;

    public SubscriptionValidator(Integer customerId, Integer servicePlanId, int usedSeats) {
        this.customerId = customerId;
        this.servicePlanId = servicePlanId;
        this.usedSeats = usedSeats;
    }

    public void validate() {
        Validate.isTrue(usedSeats >= 0 && usedSeats < 999999, "used seats's should be more or equal 0 and less 999999");
    }
}
