package ru.nsu.fit.endpoint.appserver.service.entity.data;

import ru.nsu.fit.endpoint.appserver.service.database.validation.PlanValidator;

public class Plan {

    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    private int minSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerSeat;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public int getMinSeats() {
        return minSeats;
    }

    public void setMinSeats(int minSeats) {
        this.minSeats = minSeats;
    }

    public int getFeePerSeat() {
        return feePerSeat;
    }

    public void setFeePerSeat(int feePerSeat) {
        this.feePerSeat = feePerSeat;
    }

    private PlanValidator validator;

    public Plan(String name, String details, int maxSeats, int minSeats, int feePerSeat) {
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerSeat = feePerSeat;

        validator = new PlanValidator(name, details, maxSeats, minSeats, feePerSeat);
        validator.validate();
    }
    public Plan(){

    }
}
