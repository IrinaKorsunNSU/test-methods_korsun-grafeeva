package ru.nsu.fit.endpoint.appserver.service.entity.data;

import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;
import ru.nsu.fit.endpoint.appserver.service.database.validation.UserValidator;
import ru.nsu.fit.endpoint.appserver.service.entity.enums.UserRoleEnum;


public class Admin extends CommonUser {

    private UserValidator validator;

    public Admin(String firstName, String lastName,
                 String login, String pass) throws PatternMatchExeption, SecurityPolicyExeption {
        super(firstName, lastName, login, pass);
        this.userRole = UserRoleEnum.ADMIN.name();
        validator = new UserValidator(firstName, lastName, login, pass);
        validator.validate();
    }

    public Admin() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public UserValidator getValidator() {
        return validator;
    }

    public void setValidator(UserValidator validator) {
        this.validator = validator;
    }


}
