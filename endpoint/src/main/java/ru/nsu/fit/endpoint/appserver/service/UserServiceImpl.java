package ru.nsu.fit.endpoint.appserver.service;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;
import ru.nsu.fit.endpoint.appserver.service.entity.data.CommonUser;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Customer;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Admin;
import ru.nsu.fit.endpoint.appserver.service.entity.data.User;
import ru.nsu.fit.endpoint.appserver.service.entity.enums.UserRoleEnum;

import static jooq.generate.Tables.ADMIN_DB;
import static jooq.generate.Tables.CUSTOMER;
import static jooq.generate.Tables.USER_DB;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private DSLContext dsl;
    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    @Override
    public CommonUser getUser(String login) throws SecurityPolicyExeption, PatternMatchExeption {

        User user = dsl.selectFrom(USER_DB).where(USER_DB.LOGIN.eq(login)).fetchOneInto(User.class);
        if (user.getUserRole().equals(UserRoleEnum.CUSTOMER.name())) {
            Customer customer = dsl.selectFrom(CUSTOMER).where(CUSTOMER.ID.eq(user.getId())).fetchOneInto(Customer.class);
            customer.setUserRole(UserRoleEnum.CUSTOMER.name());
            customer.setPass(shaPasswordEncoder.encodePassword("password", null));
            return customer;
        } else {
            Admin admin = dsl.selectFrom(ADMIN_DB).where(ADMIN_DB.ID.eq(user.getId())).fetchOneInto(Admin.class);
            admin.setUserRole(UserRoleEnum.ADMIN.name());
            admin.setPass(shaPasswordEncoder.encodePassword("password", null));
            return admin;
        }
    }

    private String getHashedPassword(String password) {
        ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
        String hashed = encoder.encodePassword(password, null);
        return hashed;
    }
}