package ru.nsu.fit.endpoint.appserver.service;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Subscription;

import java.util.List;

import static jooq.generate.Tables.SUBSCRIPTION;

@Service
@Transactional("transactionManager")
public class SubscriptionService {
    @Autowired
    private DSLContext dsl;
    @Autowired
    private CustomerService customerService;

    @Secured("ROLE_CUSTOMER")
    public List<Record> getAllCustomerSubscriptions() {
        return dsl.select()
                .from(SUBSCRIPTION)
                .where(SUBSCRIPTION.CUSTOMER_ID.eq(customerService.getId()))
                .fetch();
    }

    @Secured("ROLE_CUSTOMER")
    public void unassign(String id) {
        dsl.deleteFrom(jooq.generate.tables.Subscription.SUBSCRIPTION)
                .where(jooq.generate.tables.Subscription.SUBSCRIPTION.ID.eq(id))
                .execute();
    }

    public void insert(Subscription subscription) {
        dsl.insertInto(SUBSCRIPTION, SUBSCRIPTION.CUSTOMER_ID, SUBSCRIPTION.USED_SEATS, SUBSCRIPTION.PLAN_ID)
                .values(subscription.getCustomerId(), subscription.getUsedSeats(), subscription.getServicePlanId())
                .execute();
    }
}
