package ru.nsu.fit.endpoint.appserver.service.database.exeptions;

/**
 * Created by Irina Korsun on 13.10.2016.
 */
public class QuantityExeption extends Exception{
    public QuantityExeption(String s) {
        super(s);
    }
}
