package ru.nsu.fit.endpoint.appserver.service.entity.data;


public class PlansInPending {
    private Integer planId;
    private Integer customerId;

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public PlansInPending(Integer planId, Integer customerId) {
        this.planId = planId;
        this.customerId = customerId;
    }

}
