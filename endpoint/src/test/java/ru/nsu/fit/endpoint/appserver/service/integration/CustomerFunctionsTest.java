package ru.nsu.fit.endpoint.appserver.service.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.nsu.fit.endpoint.appserver.service.CustomerService;
import ru.nsu.fit.endpoint.appserver.service.PlanService;
import ru.nsu.fit.endpoint.appserver.service.SubscriptionService;
import ru.nsu.fit.endpoint.config.DatabaseConfig;
import ru.nsu.fit.endpoint.config.SecurityConfig;
import ru.nsu.fit.endpoint.config.WebAppConfig;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@EnableConfigurationProperties
@ContextConfiguration(classes = {WebAppConfig.class, SecurityConfig.class, DatabaseConfig.class})
public class CustomerFunctionsTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    CustomerService customerService;

    @Autowired
    PlanService planService;

    @Autowired
    SubscriptionService subscriptionService;

    MockMvc mockMvc;

    @Step("Инициализация мока контекста")
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog(String attachName, String message) {
        return message;
    }
    @Step("Аутентификация пользователя с нужной ролью")
    private void authBy(String role) {
        Set<GrantedAuthority> roles = new HashSet();
        roles.add(new SimpleGrantedAuthority(role));
        UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken("user", "pass", roles);
        SecurityContextHolder.getContext().setAuthentication(principal);
        saveTextLog("log","Пользователь с ролью: "+ role + " успешно аутентифицирован");
    }

    @Title("Тест проверки на доступ к кастомеру анонимному пользователю")
    @Test(expected = AccessDeniedException.class)
    public void findByLogin() throws Exception {
        authBy("ROLE_ANONYMOUS");
        customerService.findByLogin();
    }

    @Title("Тест проверки на доступ кастомера к информации на самого себя")
    @Test
    public void getCustomerInfo() throws Exception {
        authBy("ROLE_CUSTOMER");
        customerService.findByLogin();
    }

    @Title("Тест проверки на доступ к информации о планах кастомеру")
    @Test
    public void getAllPlansByCustomer() throws Exception {
        authBy("ROLE_CUSTOMER");
        planService.getAllCustomerPlans();
    }

    @Title("Тест проверки на доступ к информации о планах анонимному пользователю")
    @Test(expected = AccessDeniedException.class)
    public void getAllPlansByAnonym() throws Exception {
        authBy("ROLE_ANONYMOUS");
        planService.getAllCustomerPlans();
    }

    @Title("Тест проверки на доступ к информации о подписках кастомеру")
    @Test
    public void getAllSubscriptionsByCustomer() {
        authBy("ROLE_CUSTOMER");
        subscriptionService.getAllCustomerSubscriptions();
    }

    @Title("Тест проверки на доступ к информации о подписках анонимному пользователю")
    @Test(expected = AccessDeniedException.class)
    public void getAllSubscriptionsByAnonym() {
        authBy("ROLE_ANONYMOUS");
        subscriptionService.getAllCustomerSubscriptions();
    }
}
