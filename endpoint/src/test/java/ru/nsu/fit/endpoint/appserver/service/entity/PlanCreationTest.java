package ru.nsu.fit.endpoint.appserver.service.entity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Plan;

public class PlanCreationTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    public PlanCreationTest() {
    }

    @Test
    public void testCreatePlanWithShortName() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("name\'s length should be more or equal 2 symbols and less or equal 128 symbols and haven\'t contains special symbols");
        new Plan("p", "details", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithLongName() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("name\'s length should be more or equal 2 symbols and less or equal 128 symbols and haven\'t contains special symbols");
        new Plan(new Character[159].toString(), "details", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithSpecialCharacters() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("name\'s length should be more or equal 2 symbols and less or equal 128 symbols and haven\'t contains special symbols");
        new Plan("plan!", "details", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithShortDetails() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("details\'s length should be more or equal 1 symbols and less or equal 1024 symbols");
        new Plan("plan", "", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithLongDetails() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("details\'s length should be more or equal 1 symbols and less or equal 1024 symbols");
        new Plan("plan", "fdssssffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdssssffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdssssfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdssssfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdssssfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdssssffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithCorrectDetails() {
        new Plan("plan", "details", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithSmallMaxSeats() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("max seats should be more or equal 1 symbols and less or equal 999999");
        new Plan("plan", "details", 0, 120, 15);
    }

    @Test
    public void testCreatePlanWithBigMaxSeats() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("max seats should be more or equal 1 symbols and less or equal 999999");
        new Plan("plan", "details", 1000000, 120, 15);
    }

    @Test
    public void testCreatePlanWithCorrectMaxSeats() {
        new Plan("plan", "details", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithSmallMinSeats() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("min seats should be more or equal 1 symbols and less or equal 999999 and min seats should be less or equal max seats");
        new Plan("plan", "details", 120, 0, 15);
    }

    @Test
    public void testCreatePlanWithBigMinSeats() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("min seats should be more or equal 1 symbols and less or equal 999999 and min seats should be less or equal max seats");
        new Plan("plan", "details", 120, 1000000, 15);
    }

    @Test
    public void testCreatePlanWithMinSeatsBiggerThanMaxSeats() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("min seats should be more or equal 1 symbols and less or equal 999999 and min seats should be less or equal max seats");
        new Plan("plan", "details", 120, 500, 15);
    }

    @Test
    public void testCreatePlanWithCorrectMinSeats() {
        new Plan("plan", "details", 360, 120, 15);
    }

    @Test
    public void testCreatePlanWithSmallFeePerUnit() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("fee per unit should be more or equal 0 symbols and less or equal 999999");
        new Plan("plan", "details", 120, 60, -1);
    }

    @Test
    public void testCreatePlanWithBigFeePerUnit() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("fee per unit should be more or equal 0 symbols and less or equal 999999");
        new Plan("plan", "details", 120, 60, 1000000);
    }

    @Test
    public void testCreatePlanWithCorrectFeePerUnit() {
        new Plan("plan", "details", 360, 120, 15);
    }

}
