package ru.nsu.fit.endpoint.appserver.service.entity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Admin;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;
import ru.nsu.fit.endpoint.appserver.service.entity.enums.UserRoleEnum;

import java.util.UUID;

/**
 * Created by Irina Korsun on 10.10.2016.
 */
public class UserCreationTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() throws PatternMatchExeption, SecurityPolicyExeption {
        new Admin("John", "Whhh", "john_wick@gmail.com", "Q&%1pass");
    }

    @Test
    public void testCreateNewCustomerWithShortPass() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less then 13," +
                " should have upper case letter, digits and special symbols");
        new Admin("John", "Wick", "john_wick@gmail.com", "123");
    }

    @Test
    public void testCreateNewCustomerWithLongPass() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less then 13," +
                " should have upper case letter, digits and special symbols");
        new Admin("John", "Wick", "john_wick@gmail.com", "123qwe123qwe%1Q");
    }

    @Test
    public void testCreateNewCustomerWithEasyPass() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less then 13," +
                " should have upper case letter, digits and special symbols");
        new Admin("John", "Wick", "john_wick@gmail.com", "123qwe");
    }

    @Test
    public void testCreateNewCustomerWithBadPasswordContainsLogin() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(SecurityPolicyExeption.class);
        expectedEx.expectMessage("Password contains login");
        new Admin("John", "Wick", "wck@com", "%S12wck@com");
    }

    @Test
    public void testCreateNewCustomerWithBadPasswordContainsFirstName() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(SecurityPolicyExeption.class);
        expectedEx.expectMessage("Password contains firstName");
        new Admin("John", "Wick", "john_wick@gmail.com", "123Johnadas%");
    }

    @Test
    public void testCreateNewCustomerWithBadPasswordContainsLastName() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(SecurityPolicyExeption.class);
        expectedEx.expectMessage("Password contains lastName");
        new Admin("John", "Wick", "john_wick@gmail.com", "123Wickadas%");
    }

    @Test
    public void testCreateNewCustomerLastNameWitespacesCheck() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("John", "As   fg  ", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLastNameLengthCheckShort() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("John", "z", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLastNameLengthCheckLong() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("John", "zasdasdasdasdasdasdasdasdasd", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLastNameUpperCase() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("John", "zasd", "john_wick@gmail.com", "Q&%1pass%");
    }


    @Test
    public void testCreateNewCustomerLastNameSpecialCharacterCheck() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("John", "z%2", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerFirstNameWitespacesCheck() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("As   fg ", "John", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerFirstNameLengthCheckShort() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("z", "Jphn", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerFirstNameLengthCheckLong() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("zsssssdasdasdasdasdasdas", "Jphn", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerFirstNameUpperCase() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("zsssss", "Jphn", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerFirstNameSpecialCharacterCheck() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Admin("z%2", "John", "john_wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLoginCheckSpecialCharacterCheck() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Admin("John", "John", "john+wick@gmail.com", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLoginCheckNoDomain() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Admin("John", "John", "john@", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLoginCheckDotEnd() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Admin("John", "John", "john@sad.", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLoginCheckOneWord() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Admin("John", "John", "john", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLoginCheckLongDomain() throws PatternMatchExeption,  SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Admin("John", "John", "john.fg@fghfhhgfhgf.ghghjghgjh.hghjg", "Q&%1pass%");
    }

    @Test
    public void testCreateNewCustomerLoginCheckSeveralDomain() throws PatternMatchExeption,  SecurityPolicyExeption {
        new Admin("John", "John", "john@dff.dsf.cdf", "Q&%1pass%");
    }
}
