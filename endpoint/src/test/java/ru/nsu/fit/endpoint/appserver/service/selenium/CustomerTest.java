package ru.nsu.fit.endpoint.appserver.service.selenium;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CustomerTest {
    private static WebDriver driver;

    @Before
    public void login() {
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/secure/");
        driver.findElement(By.id("enter_button")).click();
        driver.findElement(By.name("j_username")).sendKeys("customer");
        driver.findElement(By.name("j_password")).sendKeys("password");
        driver.findElement(By.id("submit")).click();
    }

    @Test
    public void customerEnterByCustomerTest() {
        Assert.assertEquals("Spring Security", driver.getTitle());
        driver.findElement(By.id("enterbycustomer")).click();
    }

    @Test(expected = NoSuchElementException.class)
    public void customerEnterByAdminTest() {
        driver.findElement(By.id("enterbyadmin")).click();
    }

    @Test
    public void getCustomerInfo(){
        driver.findElement(By.id("enterbycustomer")).click();
        Assert.assertEquals(true, driver.findElement(By.id("customerinfo")).getText().contains("customer"));
    }

    @After
    public void closeDriver(){
        driver.close();
    }

}
