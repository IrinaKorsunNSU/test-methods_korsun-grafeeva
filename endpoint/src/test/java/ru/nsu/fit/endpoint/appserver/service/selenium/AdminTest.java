package ru.nsu.fit.endpoint.appserver.service.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AdminTest {
    private static WebDriver driver;

    @Before
    public void login() {
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/secure/");
        driver.findElement(By.id("enter_button")).click();
        driver.findElement(By.name("j_username")).sendKeys("admin");
        driver.findElement(By.name("j_password")).sendKeys("password");
        driver.findElement(By.id("submit")).click();
    }

    @Test
    public void adminEnterByAdminTest(){
        driver.findElement(By.id("enterbyadmin")).click();
    }

    @Test(expected = NoSuchElementException.class)
    public void adminEnterByCustomerTest() {
        driver.findElement(By.id("enterbycustomer")).click();
    }
    @Test
    public void createCustomerTest(){
        driver.findElement(By.id("enterbyadmin")).click();
        driver.findElement(By.id("createcustomer")).click();
        driver.findElement(By.id("firstname")).sendKeys("Irina");
        driver.findElement(By.id("lastname")).sendKeys("Korsun");
        driver.findElement(By.id("login")).sendKeys("irina.korsun.nsu@gmail.com");
        driver.findElement(By.id("pass")).sendKeys("123");
        driver.findElement(By.id("create")).click();
    }

    @After
    public void closeDriver(){
       // driver.close();
    }


}
