package ru.nsu.fit.endpoint.appserver.service.entity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Subscription;

import java.util.UUID;

public class SubscriptionCreationTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    public SubscriptionCreationTest() {
    }

    @Test
    public void testCreateSubscription() {
        new Subscription(1, 1, 42);
    }

    @Test
    public void testCreateSubscriptionWithSmallUsedSeats() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("used seats\'s should be more or equal 0 and less 999999");
        new Subscription(1, 1, -2);
    }

    @Test
    public void testCreateSubscriptionWithBigUsedSeats() {
        this.expectedEx.expect(IllegalArgumentException.class);
        this.expectedEx.expectMessage("used seats\'s should be more or equal 0 and less 999999");
        new Subscription(1, 1, 999999);
    }
}
