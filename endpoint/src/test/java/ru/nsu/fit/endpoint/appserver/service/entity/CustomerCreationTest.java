package ru.nsu.fit.endpoint.appserver.service.entity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Customer;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.QuantityExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerCreationTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        new Customer("John", "Whhh", "john_wick@gmail.com", "Q&%1pass", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() throws PatternMatchExeption, QuantityExeption, SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less then 13," +
                " should have upper case letter, digits and special symbols");
        new Customer("John", "Wick", "john_wick@gmail.com", "123", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less then 13," +
                " should have upper case letter, digits and special symbols");
        new Customer("John", "Wick", "john_wick@gmail.com", "123qwe123qwe%1Q", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less then 13," +
                " should have upper case letter, digits and special symbols");
        new Customer("John", "Wick", "john_wick@gmail.com", "123qwe", 0);
    }

    @Test
    public void testCreateNewCustomerWithBadPasswordContainsLogin() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(SecurityPolicyExeption.class);
        expectedEx.expectMessage("Password contains login");
        new Customer("John", "Wick", "wck@com", "%S12wck@com", 0);
    }

    @Test
    public void testCreateNewCustomerWithBadPasswordContainsFirstName() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(SecurityPolicyExeption.class);
        expectedEx.expectMessage("Password contains firstName");
        new Customer("John", "Wick", "john_wick@gmail.com", "123Johnadas%", 0);
    }

    @Test
    public void testCreateNewCustomerWithBadPasswordContainsLastName() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(SecurityPolicyExeption.class);
        expectedEx.expectMessage("Password contains lastName");
        new Customer("John", "Wick", "john_wick@gmail.com", "123Wickadas%", 0);
    }

    @Test
    public void testCreateNewCustomerLastNameWitespacesCheck() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("John", "As   fg  ", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLastNameLengthCheckShort() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("John", "z", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLastNameLengthCheckLong() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("John", "zasdasdasdasdasdasdasdasdasd", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLastNameUpperCase() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("John", "zasd", "john_wick@gmail.com", "Q&%1pass%", 0);
    }


    @Test
    public void testCreateNewCustomerLastNameSpecialCharacterCheck() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("lastName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("John", "z%2", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerFirstNameWitespacesCheck() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("As   fg ", "John", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerFirstNameLengthCheckShort() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("z", "Jphn", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerFirstNameLengthCheckLong() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("zsssssdasdasdasdasdasdas", "Jphn", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerFirstNameUpperCase() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("zsssss", "Jphn", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerFirstNameSpecialCharacterCheck() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("firstName's length should be more or equal 2 symbols and less or equal 12 symbols," +
                " starts with Upper case and doesn't contain digests and whitespaces");
        new Customer("z%2", "John", "john_wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLoginCheckSpecialCharacterCheck() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Customer("John", "John", "john+wick@gmail.com", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLoginCheckNoDomain() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Customer("John", "John", "john@", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLoginCheckDotEnd() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Customer("John", "John", "john@sad.", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLoginCheckOneWord() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Customer("John", "John", "john", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLoginCheckLongDomain() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        expectedEx.expect(PatternMatchExeption.class);
        expectedEx.expectMessage("Incorrect login");
        new Customer("John", "John", "john.fg@fghfhhgfhgf.ghghjghgjh.hghjg", "Q&%1pass%", 0);
    }

    @Test
    public void testCreateNewCustomerLoginCheckSeveralDomain() throws PatternMatchExeption, QuantityExeption,
            SecurityPolicyExeption {
        new Customer("John", "John", "john@dff.dsf.cdf", "Q&%1pass%", 0);
    }
}
