package ru.nsu.fit.endpoint.appserver.service.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.nsu.fit.endpoint.appserver.service.CustomerService;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.PatternMatchExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.QuantityExeption;
import ru.nsu.fit.endpoint.appserver.service.database.exeptions.SecurityPolicyExeption;
import ru.nsu.fit.endpoint.appserver.service.entity.data.Customer;
import ru.nsu.fit.endpoint.config.DatabaseConfig;
import ru.nsu.fit.endpoint.config.SecurityConfig;
import ru.nsu.fit.endpoint.config.WebAppConfig;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@EnableConfigurationProperties
@ContextConfiguration(classes = {WebAppConfig.class, SecurityConfig.class, DatabaseConfig.class})
public class AdminFunctionsTest {

    @Autowired
    private WebApplicationContext context;

    MockMvc mockMvc;

    @Autowired
    CustomerService customerService;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Step("Аутентификация пользователя с нужной ролью")
    private void authBy(String role) {
        Set<GrantedAuthority> roles = new HashSet();
        roles.add(new SimpleGrantedAuthority(role));
        UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken("user", "pass", roles);
        SecurityContextHolder.getContext().setAuthentication(principal);
    }

    @Title("Тест проверки на доступ к списку кастомеров анонимному пользователю")
    @Test(expected = AccessDeniedException.class)
    public void getCustomerByAnonym() throws Exception {
        authBy("ROLE_ANONYMOUS");
        customerService.findAll();
    }

    @Title("Тест проверки на доступ к списку кастомеров админу")
    @Test
    public void getCustomerByAdmin() throws Exception {
        authBy("ROLE_ADMIN");
        customerService.findAll();
    }


    @Title("Тест проверки на доступ к добавлению кастомероа админу")
    @Transactional
    @Rollback
    @Test
    public void addCustomerByAdmin() throws QuantityExeption, PatternMatchExeption, SecurityPolicyExeption {
        authBy("ROLE_ADMIN");
        Customer customer = new Customer("John", "John", "john@dff.dsf.cdf", "Q&%1pass%", 0);
        customerService.insert(customer.getFirstName(), customer.getLastName(), customer.getLogin(), customer.getPass(), customer.getMoney());
    }

    @Title("Тест проверки на доступ к добавлению кастомероа анонимному пользователю")
    @Transactional
    @Rollback
    @Test(expected = AccessDeniedException.class)
    public void addCustomerByAnonym() throws QuantityExeption, PatternMatchExeption, SecurityPolicyExeption {
        authBy("ROLE_ANONYMOUS");
        Customer customer = new Customer("John", "John", "john@dff.dsf.cdf", "Q&%1pass%", 0);
        customerService.insert(customer.getFirstName(), customer.getLastName(), customer.getLogin(), customer.getPass(), customer.getMoney());
    }

}
