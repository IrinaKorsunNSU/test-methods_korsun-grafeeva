package ru.nsu.fit.endpoint.appserver.service.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.nsu.fit.endpoint.appserver.service.CustomerService;
import ru.nsu.fit.endpoint.config.DatabaseConfig;
import ru.nsu.fit.endpoint.config.SecurityConfig;
import ru.nsu.fit.endpoint.config.WebAppConfig;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@EnableConfigurationProperties
@ContextConfiguration(classes = {WebAppConfig.class, SecurityConfig.class, DatabaseConfig.class})
public class AuthTest {

    @Autowired
    private WebApplicationContext context;

    MockMvc mockMvc;

    @Autowired
    CustomerService customerService;

    @Step("Аутентификация пользователя с нужной ролью")
    private void authBy(String role) {
        Set<GrantedAuthority> roles = new HashSet();
        roles.add(new SimpleGrantedAuthority(role));
        UsernamePasswordAuthenticationToken principal = new UsernamePasswordAuthenticationToken("user", "pass", roles);
        SecurityContextHolder.getContext().setAuthentication(principal);
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Title("Тест проверки на доступ к директории /admin/ админу")
    @Test
    public void homeByAdmin() throws Exception {
        authBy("ROLE_ADMIN");
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/admin/");
        mockMvc.perform(request);
    }

    @Title("Тест проверки на доступ к директории /customer/ кастомеру")
    @Test
    public void homeByCustomer() throws Exception {
        authBy("ROLE_CUSTOMER");
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/customer/");
        mockMvc.perform(request);
    }

    @Title("Тест проверки на доступ к директории /admin/ анонимному пользователю")
    @Test(expected = NestedServletException.class)
    public void homeByAnonymous() throws Exception {
        authBy("ROLE_CUSTOMER");
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/admin/");
        mockMvc.perform(request);
    }

}