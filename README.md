#  Для чего все это? #

*Это исходный код лабораторной работы по методам тестирования

Ссылка на тест-кейсы: [Тест-кейсы](https://docs.google.com/document/d/1AIB2o90WO80lQimPlQdvqEwEHs74wcgf71RWJdg88Zg/edit?usp=sharing)

# Как запустить проект у себя? #

## Настройка базы данных ##
В разработке проекта использовался Oracle 12
Для создания таблиц вам поможет содержимое файла src\main\resources\db_create
Заполните таблицы Subscroptions и Plan случайными значениями
Логин и пароль для авторизации под админским пользователем и под пользователем customera вшиты в код, пожтому таблицы customer и admin_db можно оставить пустыми

## Запуск проекта ##
Соберите проект (maven)
Для деплоя необходимо настроить tomcat так как показано на рисунках

![Screenshot_3.png](https://bitbucket.org/repo/kEx87a/images/2753953148-Screenshot_3.png)
![Screenshot_4.png](https://bitbucket.org/repo/kEx87a/images/3635008740-Screenshot_4.png)

Если сборка и деплой прошел успешно то по ссылке http://localhost:8080/secure/ можно увидеть кнопку перехода на страницу авторизации
На данный момент можно авторизоваться следующим образом:

* **login: mycustomer password: mypassword (ROLE_CUSTOMER)**
* **login: user password: password (ROLE_ADMIN)**

Авторизация из данных базы пока отключена, так как она требует доработки

## Просмотр статистики тестов ##
На сегодняшний день посмотреть статистику по тестам идет через jetty
Для этого необходимо остановить tomcat если он запущен
Далее запустить команды maven test, site и плагин jetty:run (который должен автоматически подгрузиться с зависимостями maven)
После запуска jetty перейти по адресу: http://localhost:8080/

#Скриншоты #
![Screenshot_2.png](https://bitbucket.org/repo/kEx87a/images/160765452-Screenshot_2.png)
![Screenshot_3.png](https://bitbucket.org/repo/kEx87a/images/708026109-Screenshot_3.png)
![Screenshot_7.png](https://bitbucket.org/repo/kEx87a/images/305550201-Screenshot_7.png)
![Screenshot_4.png](https://bitbucket.org/repo/kEx87a/images/1104383822-Screenshot_4.png)
![Screenshot_5.png](https://bitbucket.org/repo/kEx87a/images/2483564131-Screenshot_5.png)
![Screenshot_6.png](https://bitbucket.org/repo/kEx87a/images/3394625276-Screenshot_6.png)
![Screenshot_8.png](https://bitbucket.org/repo/kEx87a/images/3111023642-Screenshot_8.png)
![Screenshot_9.png](https://bitbucket.org/repo/kEx87a/images/3912953422-Screenshot_9.png)
![Screenshot_10.png](https://bitbucket.org/repo/kEx87a/images/1529906689-Screenshot_10.png)

## С кем связываться если есть проблемы? ##
Ирина: irina.korsun.nsu@gmail.com
89607846758